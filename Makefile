lang=${LANG}

project_root_dir=$(dir $(abspath $(lastword $(MAKEFILE_LIST))))

meson_root_dir=${project_root_dir}meson
meson_build_dir=${meson_root_dir}/build
meson_install_dir=${meson_root_dir}/install

flatpak_id=tech.orzechowski.suma
flatpak_manifest_file=${flatpak_id}.json
flatpak_bundle_file=${flatpak_id}.flatpak
flatpak_manifest_path=${project_root_dir}${flatpak_manifest_file}
flatpak_root_dir=${project_root_dir}flatpak
flatpak_build_dir=${flatpak_root_dir}/build
flatpak_state_dir=${flatpak_root_dir}/state
flatpak_repo_dir=${flatpak_root_dir}/repo

flatpak_warning='!!! WARNING !!! \
	This will result in oom killer taking down rust-analyzer \
	along with its parent process due to combination \
	of https://github.com/rust-analyzer/rust-analyzer/issues/7755 \
	and https://github.com/rust-analyzer/rust-analyzer/issues/8161. \
	You should run `make flatpak-clean` before re-running rust-analyzer.'

${meson_build_dir}:
	mkdir -p '${meson_build_dir}'

${meson_install_dir}:
	mkdir -p '${meson_install_dir}'

configure-dev: ${meson_build_dir}
	meson setup '${meson_build_dir}' --buildtype debug --prefix '${meson_install_dir}'

configure-dev-release: ${meson_build_dir}
	meson setup '${meson_build_dir}' --buildtype release --prefix '${meson_install_dir}'

configure-prod: ${meson_build_dir}
	meson setup '${meson_build_dir}' --buildtype debug

configure-prod-release: ${meson_build_dir}
	meson setup '${meson_build_dir}' --buildtype release

reconfigure-dev: ${meson_build_dir}
	meson setup '${meson_build_dir}' --buildtype debug --prefix '${meson_install_dir}' --reconfigure

reconfigure-dev-release: ${meson_build_dir}
	meson setup '${meson_build_dir}' --buildtype release --prefix '${meson_install_dir}' --reconfigure

reconfigure-prod: ${meson_build_dir}
	meson setup '${meson_build_dir}' --buildtype debug --reconfigure

reconfigure-prod-release: ${meson_build_dir}
	meson setup '${meson_build_dir}' --buildtype release --reconfigure

compile:
	meson compile -C '${meson_build_dir}'

run: compile
	LANG='${lang}' XDG_DATA_DIRS='${meson_install_dir}/share:${XDG_DATA_DIRS}' '${meson_install_dir}/bin/suma'

install: ${meson_install_dir} compile
	meson install -C '${meson_build_dir}'

clean:
	meson compile -C '${meson_build_dir}' --clean

wipe:
	meson setup '${meson_build_dir}' --wipe

${flatpak_build_dir}:
	mkdir -p '${flatpak_build_dir}'

${flatpak_state_dir}:
	mkdir -p '${flatpak_state_dir}'

${flatpak_repo_dir}:
	mkdir -p '${flatpak_repo_dir}'

.PHONY: flatpak-compile
flatpak-compile: ${flatpak_build_dir} ${flatpak_state_dir}
	@echo ${flatpak_warning}

	flatpak-builder \
		--arch=x86_64 \
		--ccache \
		--force-clean \
		--state-dir '${flatpak_state_dir}' \
		--repo '${flatpak_repo_dir}' \
		'${flatpak_build_dir}' \
		'${flatpak_manifest_path}'

	@echo ${flatpak_warning}

flatpak-run:
	flatpak-builder \
		--arch=x86_64 \
		--ccache \
		--run \
		'${flatpak_build_dir}' \
		'${flatpak_manifest_path}' \
		suma

flatpak-bundle:
	flatpak build-bundle \
		--arch=x86_64 \
		'${flatpak_repo_dir}' \
		'${flatpak_build_dir}/${flatpak_bundle_file}' \
		'${flatpak_id}' \
		master

flatpak-install:
	flatpak --user install '${flatpak_build_dir}/${flatpak_bundle_file}'

flatpak-uninstall:
	flatpak --user uninstall '${flatpak_id}'

flatpak-clean:
	rm -rf '${flatpak_build_dir}'
	rm -rf '${flatpak_state_dir}'
	rm -rf '${flatpak_repo_dir}'
