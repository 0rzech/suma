use crate::open_dialog::{OpenDialog, OpenDialogInput, OpenDialogOutput, OpenDialogSettings};
use gtk::prelude::*;
use relm4::{
    gtk, Component, ComponentController, ComponentParts, ComponentSender, Controller,
    SimpleComponent,
};
use std::path::PathBuf;

pub struct OpenButtonSettings {
    pub label: String,
}

#[derive(Debug)]
pub enum OpenButtonInput {
    ShowDialog,
    OpenFile(PathBuf),
    SetSensitive(bool),
    Reset,
}

impl From<OpenDialogOutput> for OpenButtonInput {
    fn from(value: OpenDialogOutput) -> Self {
        match value {
            OpenDialogOutput::Accept(path) => Self::OpenFile(path),
        }
    }
}

#[derive(Debug)]
pub enum OpenButtonOutput {
    OpenFile(PathBuf),
}

#[tracker::track]
pub struct OpenButton {
    #[do_not_track]
    default_button_label: String,
    button_label: String,
    button_sensitive: bool,
    #[do_not_track]
    open_dialog: Controller<OpenDialog>,
}

#[relm4::component(pub)]
impl SimpleComponent for OpenButton {
    type Init = (OpenButtonSettings, OpenDialogSettings);
    type Input = OpenButtonInput;
    type Output = OpenButtonOutput;

    fn init(
        settings: Self::Init,
        root: &Self::Root,
        sender: ComponentSender<Self>,
    ) -> ComponentParts<Self> {
        let (button_settings, dialog_settings) = settings;

        let model = Self {
            default_button_label: button_settings.label.clone(),
            button_label: button_settings.label.clone(),
            button_sensitive: true,
            open_dialog: OpenDialog::builder()
                .transient_for_native(root)
                .launch(dialog_settings)
                .forward(sender.input_sender(), Self::Input::from),
            tracker: 0,
        };

        let widgets = view_output!();

        ComponentParts { model, widgets }
    }

    view! {
        #[root]
        gtk::Button::with_mnemonic("") {
            #[track = "model.changed(OpenButton::button_label())"]
            set_label: model.button_label.as_ref(),

            #[track = "model.changed(OpenButton::button_sensitive())"]
            set_sensitive: model.button_sensitive,

            connect_clicked[sender] => move |_| {
                sender.input(Self::Input::ShowDialog)
            },
        }
    }

    fn update(&mut self, message: Self::Input, sender: ComponentSender<Self>) {
        self.reset();
        match message {
            Self::Input::ShowDialog => self.open_dialog.emit(OpenDialogInput::Open),
            Self::Input::OpenFile(path) => {
                let label = match path.file_name() {
                    Some(name) => name.to_string_lossy().to_string(),
                    None => self.button_label.to_string(),
                };
                self.set_button_label(label);
                sender.output(Self::Output::OpenFile(path)).unwrap();
            }
            Self::Input::SetSensitive(sensitive) => self.set_button_sensitive(sensitive),
            Self::Input::Reset => self.set_button_label(self.default_button_label.clone()),
        }
    }
}