mod about_dialog;
mod algorithm;
mod config;
mod header;
mod localization;
mod open_button;
mod open_dialog;
mod worker;

use about_dialog::{AboutDialog, AboutDialogInput, AboutDialogSettings};
use algorithm::{Algorithm, AlgorithmTextDecorator};
use fluent_bundle::{FluentArgs, FluentBundle, FluentResource};
use gtk::prelude::*;
use header::{Header, HeaderInput, HeaderOutput, HeaderSettings};
use localization::FluentBundleExt;
use open_button::{OpenButton, OpenButtonInput, OpenButtonOutput, OpenButtonSettings};
use open_dialog::OpenDialogSettings;
use relm4::{
    Component, ComponentController, ComponentParts, ComponentSender, Controller, RelmApp,
    SimpleComponent, WorkerController,
};
use std::{env, path::PathBuf};
use worker::{Verifier, VerifierInput, VerifierOutput};

struct AppInit {
    file: Option<PathBuf>,
    algorithm: Algorithm,
    checksum: String,
    progress: f32,
    activity: Activity,
    result: String,
    localization: FluentBundle<FluentResource>,
}

#[derive(Debug)]
enum AppInput {
    ResetState,
    ShowAboutDialog,
    SelectFile(PathBuf),
    SelectAlgorithm(Algorithm),
    SetChecksum(String),
    ToggleWork,
    UpdateProgress(f32),
    HandleFinish(Result<String, String>),
    HandleCancel,
}

impl From<HeaderOutput> for AppInput {
    fn from(value: HeaderOutput) -> Self {
        match value {
            HeaderOutput::ResetState => Self::ResetState,
            HeaderOutput::ShowAboutDialog => Self::ShowAboutDialog,
        }
    }
}

impl From<OpenButtonOutput> for AppInput {
    fn from(value: OpenButtonOutput) -> Self {
        match value {
            OpenButtonOutput::OpenFile(path) => Self::SelectFile(path),
        }
    }
}

impl From<VerifierOutput> for AppInput {
    fn from(value: VerifierOutput) -> Self {
        match value {
            VerifierOutput::ProgressUpdated(progress) => Self::UpdateProgress(progress),
            VerifierOutput::WorkFinished(result) => Self::HandleFinish(result),
            VerifierOutput::WorkCanceled => Self::HandleCancel,
        }
    }
}

#[derive(Clone, Copy, PartialEq)]
enum Activity {
    Calculate,
    Verify,
    Working,
}

#[tracker::track]
struct App {
    file: Option<PathBuf>,
    algorithm: Algorithm,
    checksum: String,
    progress: f32,
    activity: Activity,
    result: String,
    #[do_not_track]
    localization: FluentBundle<FluentResource>,
    #[do_not_track]
    header: Controller<Header>,
    #[do_not_track]
    open_button: Controller<OpenButton>,
    #[do_not_track]
    about_dialog: Controller<AboutDialog>,
    #[do_not_track]
    worker: WorkerController<Verifier>,
}

const DEFAULT_MARGIN: i32 = 2;
const DEFAULT_WINDOW_WIDTH: i32 = 500;
const DEFAULT_WINDOW_HEIGHT: i32 = 0;

#[relm4::component]
impl SimpleComponent for App {
    type Init = AppInit;
    type Input = AppInput;
    type Output = ();

    fn init(
        init: Self::Init,
        root: &Self::Root,
        sender: ComponentSender<Self>,
    ) -> ComponentParts<Self> {
        let header_settings = HeaderSettings {
            reset_button_tooltip: Some(init.localization.get_message_attribute(
                "header",
                "reset_tooltip",
                None,
            )),
            about_button_tooltip: Some(init.localization.get_message_attribute(
                "header",
                "about_tooltip",
                None,
            )),
        };

        let open_button_settings = OpenButtonSettings {
            label: init
                .localization
                .get_message_attribute("file_chooser", "choose_file", None),
        };

        let open_dialog_settings = OpenDialogSettings {
            accept_label: init
                .localization
                .get_message_attribute("file_chooser", "choose", None),
            cancel_label: init
                .localization
                .get_message_attribute("file_chooser", "cancel", None),
            create_folders: false,
            filters: vec![],
            modal: true,
        };

        let about_dialog_settings = AboutDialogSettings {
            logo_icon_name: Some(config::LOGO_ICON_NAME.to_string()),
            comments: Some(init.localization.get_message_attribute(
                "about_dialog",
                "description",
                None,
            )),
            version: Some(config::VERSION.to_string()),
            license_type: config::LICENSE_TYPE,
            copyright: Some(init.localization.get_message_attribute(
                "about_dialog",
                "copyright",
                None,
            )),
            authors: config::AUTHORS.iter().map(|&a| a.into()).collect(),
            translator_credits: Some(config::TRANSLATOR_CREDITS.join("\n")),
            website: Some(config::WEBSITE.to_string()),
            ..AboutDialogSettings::new(config::PROGRAM_NAME.to_string())
        };

        let model = App {
            file: init.file,
            algorithm: init.algorithm,
            checksum: init.checksum,
            progress: init.progress,
            activity: init.activity,
            result: init.result,
            localization: init.localization,
            header: Header::builder()
                .launch(header_settings)
                .forward(sender.input_sender(), Self::Input::from),
            open_button: OpenButton::builder()
                .launch((open_button_settings, open_dialog_settings))
                .forward(sender.input_sender(), Self::Input::from),
            about_dialog: AboutDialog::builder()
                .transient_for(root)
                .launch(about_dialog_settings)
                .detach(),
            worker: Verifier::builder()
                .detach_worker(())
                .forward(sender.input_sender(), Self::Input::from),
            tracker: 0,
        };

        let widgets = view_output!();

        widgets
            .main_window
            .set_default_size(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT);

        let file_chooser_button = model.open_button.widget();
        file_chooser_button.set_valign(gtk::Align::Center);
        file_chooser_button.set_margin_top(DEFAULT_MARGIN);
        file_chooser_button.set_margin_bottom(DEFAULT_MARGIN);
        file_chooser_button.set_margin_start(DEFAULT_MARGIN);
        file_chooser_button.set_margin_end(DEFAULT_MARGIN);

        let size_group = gtk::SizeGroup::new(gtk::SizeGroupMode::Both);
        size_group.add_widget(&widgets.algorithm_combo);
        size_group.add_widget(&widgets.verify_cancel_button);

        if let Some(path) = &model.file {
            model
                .open_button
                .emit(OpenButtonInput::OpenFile(path.clone()));
        }
        ComponentParts { model, widgets }
    }

    view! {
        #[root]
        #[name = "main_window"]
        gtk::ApplicationWindow {
            set_title: Some(config::PROGRAM_NAME),
            set_titlebar: Some(model.header.widget()),

            gtk::Box {
                set_orientation: gtk::Orientation::Vertical,
                set_margin_top: DEFAULT_MARGIN,
                set_margin_bottom: DEFAULT_MARGIN,
                set_margin_start: DEFAULT_MARGIN,
                set_margin_end: DEFAULT_MARGIN,

                append = &gtk::Grid {
                    set_row_homogeneous: true,
                    set_column_homogeneous: true,
                    set_vexpand: true,

                    attach[0, 0, 1, 1]: model.open_button.widget(),

                    attach[0, 1, 1, 1] = &gtk::Box {
                        #[name = "algorithm_combo"]
                        append = &gtk::ComboBoxText {
                            append_text: Algorithm::Md5.to_string(&model).as_str(),
                            append_text: Algorithm::Sha1.to_string(&model).as_str(),
                            append_text: Algorithm::Sha224.to_string(&model).as_str(),
                            append_text: Algorithm::Sha256.to_string(&model).as_str(),
                            append_text: Algorithm::Sha384.to_string(&model).as_str(),
                            append_text: Algorithm::Sha512.to_string(&model).as_str(),
                            append_text: Algorithm::Sha3_224.to_string(&model).as_str(),
                            append_text: Algorithm::Sha3_256.to_string(&model).as_str(),
                            append_text: Algorithm::Sha3_384.to_string(&model).as_str(),
                            append_text: Algorithm::Sha3_512.to_string(&model).as_str(),

                            set_tooltip_text: Some(model.localization.get_message_attribute(
                                "main",
                                "algorithm_tooltip",
                                None
                            ).as_str()),

                            #[track = "model.changed(App::algorithm())"]
                            set_active: Some(model.algorithm as u32),

                            #[track = "model.changed(App::activity())"]
                            set_sensitive: model.activity != Activity::Working,

                            connect_changed[sender] => move |combo_box| {
                                let algorithm = Algorithm::from_str(combo_box.active_text().unwrap().as_str());
                                sender.input(Self::Input::SelectAlgorithm(algorithm))
                            },

                            set_valign: gtk::Align::Center,
                            set_margin_top: DEFAULT_MARGIN,
                            set_margin_bottom: DEFAULT_MARGIN,
                            set_margin_start: DEFAULT_MARGIN,
                            set_margin_end: DEFAULT_MARGIN,
                        },

                        append = &gtk::Entry {
                            set_placeholder_text: Some(model.localization.get_message_attribute(
                                "main",
                                "edit_placeholder",
                                None
                            ).as_str()),

                            #[track = "model.changed(App::checksum())"]
                            set_text: model.checksum.as_str(),
                            #[track = "model.changed(App::activity())"]
                            set_sensitive: model.activity != Activity::Working,

                            connect_changed[sender] => move |entry| {
                                sender.input(Self::Input::SetChecksum(entry.buffer().text()))
                            },

                            set_hexpand: true,
                            set_valign: gtk::Align::Center,
                            set_margin_top: DEFAULT_MARGIN,
                            set_margin_bottom: DEFAULT_MARGIN,
                            set_margin_start: DEFAULT_MARGIN,
                            set_margin_end: DEFAULT_MARGIN,
                        },
                    },

                    attach[0, 2, 1, 1] = &gtk::Box {
                        append = &gtk::ProgressBar {
                            set_show_text: true,
                            set_pulse_step: 0.05,

                            #[track = "model.changed(App::progress())"]
                            set_fraction: model.progress as f64,
                            set_hexpand: true,
                            set_valign: gtk::Align::Center,
                            set_margin_top: DEFAULT_MARGIN,
                            set_margin_bottom: DEFAULT_MARGIN,
                            set_margin_start: DEFAULT_MARGIN,
                            set_margin_end: DEFAULT_MARGIN,
                        },

                        #[name = "verify_cancel_button"]
                        append = &gtk::Button::with_mnemonic("") {
                            #[track = "model.changed(App::activity())"]
                            set_label: {
                                let key = match model.activity {
                                    Activity::Calculate => "calculate",
                                    Activity::Verify => "verify",
                                    Activity::Working => "cancel",
                                };
                                model.localization.get_message_attribute("main", key, None).as_str()
                            },

                            #[track = "model.changed(App::file())"]
                            set_sensitive: model.file.is_some(),

                            connect_clicked[sender] => move |_| {
                                sender.input(Self::Input::ToggleWork)
                            },

                            set_valign: gtk::Align::Center,
                            set_margin_top: DEFAULT_MARGIN,
                            set_margin_bottom: DEFAULT_MARGIN,
                            set_margin_start: DEFAULT_MARGIN,
                            set_margin_end: DEFAULT_MARGIN,
                        },
                    },
                },

                #[name = "status_bar"]
                append = &gtk::Label {
                    #[track = "model.changed(App::result())"]
                    set_markup: model.result.as_str(),
                    set_halign: gtk::Align::Fill,
                    set_margin_top: DEFAULT_MARGIN,
                    set_margin_bottom: DEFAULT_MARGIN,
                    set_margin_start: DEFAULT_MARGIN,
                    set_margin_end: DEFAULT_MARGIN,
                },
            },
        }
    }

    fn update(&mut self, message: Self::Input, _sender: ComponentSender<Self>) {
        self.reset();
        match message {
            Self::Input::ResetState => {
                self.set_file(None);
                self.set_checksum("".to_string());
                self.set_progress(0.0);
                self.set_result("".to_string());
                self.open_button.emit(OpenButtonInput::Reset);
            }
            Self::Input::ShowAboutDialog => {
                self.about_dialog.emit(AboutDialogInput::Show);
            }
            Self::Input::SelectFile(path) => self.set_file(Some(path)),
            Self::Input::SelectAlgorithm(algorithm) => self.algorithm = algorithm,
            Self::Input::SetChecksum(checksum) => {
                self.checksum = checksum;
                self.set_activity(self.awaiting_activity());
            }
            Self::Input::ToggleWork => {
                if self.activity == Activity::Working {
                    self.worker.emit(VerifierInput::Stop);
                } else {
                    self.handle_work_toggle(Activity::Working);
                    self.worker.emit(VerifierInput::Verify {
                        file: self.file.clone().unwrap(),
                        algorithm: self.algorithm,
                    });
                }
            }
            Self::Input::UpdateProgress(progress) => self.set_progress(progress),
            Self::Input::HandleFinish(Ok(hash)) => {
                if self.checksum.is_empty() {
                    self.set_checksum(hash);
                    self.set_result("".to_string());
                } else {
                    let message = if self.checksum == hash {
                        App::result_ok(&self.localization.get_message_attribute(
                            "status",
                            "checksums_match",
                            None,
                        ))
                    } else {
                        App::result_err(&self.localization.get_message_attribute(
                            "status",
                            "checksums_do_not_match",
                            None,
                        ))
                    };
                    self.set_result(message);
                }
                self.handle_work_toggle(self.awaiting_activity());
            }
            Self::Input::HandleFinish(Err(error)) => {
                self.set_result(App::result_err(&error));
                self.handle_work_toggle(self.awaiting_activity());
            }
            Self::Input::HandleCancel => {
                self.set_progress(0.0);
                self.set_result("".to_string());
                self.handle_work_toggle(self.awaiting_activity());
            }
        }
    }
}

impl App {
    fn result_ok(message: &String) -> String {
        format!("<span foreground=\"darkgreen\">{}</span>", message)
    }

    fn result_err(message: &String) -> String {
        format!("<span foreground=\"red\">{}</span>", message)
    }

    fn awaiting_activity(&self) -> Activity {
        if self.checksum.is_empty() {
            Activity::Calculate
        } else {
            Activity::Verify
        }
    }

    fn handle_work_toggle(&mut self, activity: Activity) {
        self.set_activity(activity);
        self.header
            .emit(HeaderInput::SetSensitive(activity != Activity::Working));
        self.open_button
            .emit(OpenButtonInput::SetSensitive(activity != Activity::Working));
    }
}

impl AlgorithmTextDecorator for App {
    fn decorate_safe_algorithm(&self, algorithm: &str) -> String {
        let mut args = FluentArgs::new();
        args.set("algorithm", algorithm);
        self.localization
            .get_message_attribute("algorithm", "safe", Some(&args))
    }

    fn decorate_moderate_algorithm(&self, algorithm: &str) -> String {
        let mut args = FluentArgs::new();
        args.set("algorithm", algorithm);
        self.localization
            .get_message_attribute("algorithm", "moderate", Some(&args))
    }

    fn decorate_compromised_algorithm(&self, algorithm: &str) -> String {
        let mut args = FluentArgs::new();
        args.set("algorithm", algorithm);
        self.localization
            .get_message_attribute("algorithm", "compromised", Some(&args))
    }
}

fn file_path_arg() -> Option<PathBuf> {
    let args: Vec<String> = env::args().skip(1).collect();
    let path = args.first().and_then(|path| {
        let p = path.trim();
        if p.is_empty() {
            None
        } else {
            Some(p)
        }
    });

    path.map(PathBuf::from)
}

fn main() {
    let file_path = file_path_arg();
    let localization = localization::load(&config::LOCALE_CONFIG).unwrap();
    RelmApp::new(&config::PROGRAM_ID).run::<App>(AppInit {
        file: file_path,
        algorithm: Algorithm::default(),
        checksum: "".to_string(),
        progress: 0.0,
        activity: Activity::Calculate,
        result: "".to_string(),
        localization,
    });
}
