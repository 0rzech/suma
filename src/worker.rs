use crate::algorithm::Algorithm;
use relm4::{ComponentSender, Worker};
use std::{fs::File, io::Read, path::PathBuf, sync::mpsc, thread};

#[derive(Debug)]
pub enum VerifierInput {
    Verify { file: PathBuf, algorithm: Algorithm },
    Stop,
}

#[derive(Debug)]
pub enum VerifierOutput {
    ProgressUpdated(f32),
    WorkFinished(Result<String, String>),
    WorkCanceled,
}

enum ChannelMsg {
    Stop,
}

pub struct Verifier {
    tx: Option<mpsc::Sender<ChannelMsg>>,
}

impl Worker for Verifier {
    type Init = ();
    type Input = VerifierInput;
    type Output = VerifierOutput;

    fn init(_init: Self::Init, _sender: ComponentSender<Self>) -> Self {
        Self { tx: None }
    }

    fn update(&mut self, message: Self::Input, sender: ComponentSender<Self>) {
        match message {
            Self::Input::Verify { file, algorithm } => {
                dbg!(&file);

                let thread_sender = sender.clone();
                let (tx, rx) = mpsc::channel();
                self.tx = Some(tx);

                let result = thread::Builder::new()
                    .name("worker".to_string())
                    .spawn(move || {
                        let mut file = match File::open(&file) {
                            Ok(file) => file,
                            Err(error) => {
                                thread_sender
                                    .output(Self::Output::WorkFinished(Err(error.to_string())))
                                    .unwrap();
                                return;
                            }
                        };

                        let metadata = match file.metadata() {
                            Ok(metadata) => metadata,
                            Err(error) => {
                                thread_sender
                                    .output(Self::Output::WorkFinished(Err(error.to_string())))
                                    .unwrap();
                                return;
                            }
                        };

                        let file_size_bytes = metadata.len();
                        let mut total_bytes_read = 0;
                        let mut buffer = vec![0; BYTE_CHUNK_SIZE];
                        let mut hasher = algorithm.new_hasher();

                        dbg!(&file_size_bytes);

                        while total_bytes_read < file_size_bytes {
                            match rx.try_recv() {
                                Ok(ChannelMsg::Stop) | Err(mpsc::TryRecvError::Disconnected) => {
                                    dbg!("Terminating!");
                                    thread_sender.output(Self::Output::WorkCanceled).unwrap();
                                    return;
                                }
                                _ => {}
                            }

                            match file.read(&mut buffer) {
                                Ok(bytes_read) => {
                                    total_bytes_read += bytes_read as u64;
                                    dbg!(&bytes_read, total_bytes_read);
                                    hasher.update(&buffer[..bytes_read]);
                                    let progress = total_bytes_read as f32 / file_size_bytes as f32;
                                    dbg!(progress);
                                    thread_sender
                                        .output(Self::Output::ProgressUpdated(progress))
                                        .unwrap();
                                }
                                Err(error) => {
                                    eprintln!("{}", error);
                                    return;
                                }
                            }
                        }

                        let hash = hex::encode(hasher.finalize());

                        dbg!(&buffer.len());
                        dbg!(&hash);

                        thread_sender
                            .output(Self::Output::WorkFinished(Ok(hash)))
                            .unwrap();
                    });

                if let Err(error) = result {
                    sender
                        .output(Self::Output::WorkFinished(Err(error.to_string())))
                        .unwrap();
                }
            }
            Self::Input::Stop => {
                if let Some(tx) = &self.tx {
                    tx.send(ChannelMsg::Stop)
                        .expect("Unable to stop worker thread");
                }
            }
        }
    }
}

const DEFAULT_BYTE_CHUNK_SIZE: u64 = 10 * 1024 * 1024;
const BYTE_CHUNK_SIZE: usize = byte_chunk_size(DEFAULT_BYTE_CHUNK_SIZE);

const fn byte_chunk_size(default: u64) -> usize {
    if default < usize::MAX as u64 {
        default as usize
    } else {
        usize::MAX
    }
}
