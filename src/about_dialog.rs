use gtk::{gdk, prelude::*};
use relm4::{gtk, ComponentParts, ComponentSender, SimpleComponent};

pub struct AboutDialogSettings {
    pub artists: Vec<String>,
    pub authors: Vec<String>,
    pub comments: Option<String>,
    pub copyright: Option<String>,
    pub documenters: Vec<String>,
    pub license: Option<String>,
    pub license_type: gtk::License,
    pub logo: Option<gdk::Paintable>,
    pub logo_icon_name: Option<String>,
    pub program_name: String,
    pub system_information: Option<String>,
    pub translator_credits: Option<String>,
    pub version: Option<String>,
    pub website: Option<String>,
    pub website_label: Option<String>,
    pub wrap_license: bool,
    pub modal: bool,
    pub forward_activate_link: bool,
}

impl AboutDialogSettings {
    pub fn new(program_name: String) -> Self {
        Self {
            artists: vec![],
            authors: vec![],
            comments: None,
            copyright: None,
            documenters: vec![],
            license: None,
            license_type: gtk::License::Unknown,
            logo: None,
            logo_icon_name: None,
            program_name,
            system_information: None,
            translator_credits: None,
            version: None,
            website: None,
            website_label: None,
            wrap_license: true,
            modal: true,
            forward_activate_link: false,
        }
    }
}

#[derive(Debug)]
pub enum AboutDialogInput {
    Show,
    Close,
}

#[derive(Debug)]
pub enum AboutDialogOutput {
    ActivateLink(String),
}

#[tracker::track]
pub struct AboutDialog {
    visible: bool,
}

#[relm4::component(pub)]
impl SimpleComponent for AboutDialog {
    type Init = AboutDialogSettings;
    type Input = AboutDialogInput;
    type Output = AboutDialogOutput;

    fn init(
        settings: Self::Init,
        root: &Self::Root,
        sender: ComponentSender<Self>,
    ) -> ComponentParts<Self> {
        let model = Self {
            visible: false,
            tracker: 0,
        };
        let widgets = view_output!();
        let dialog = &widgets.about_dialog;

        if !settings.artists.is_empty() {
            dialog.set_artists(
                settings
                    .artists
                    .iter()
                    .map(String::as_str)
                    .collect::<Vec<_>>()
                    .as_slice(),
            );
        }

        if !settings.authors.is_empty() {
            dialog.set_authors(
                settings
                    .authors
                    .iter()
                    .map(String::as_str)
                    .collect::<Vec<_>>()
                    .as_slice(),
            );
        }

        if !settings.documenters.is_empty() {
            dialog.set_documenters(
                settings
                    .documenters
                    .iter()
                    .map(String::as_str)
                    .collect::<Vec<_>>()
                    .as_slice(),
            );
        }

        if let Some(comments) = &settings.comments {
            dialog.set_comments(Some(comments));
        }

        if let Some(copyright) = &settings.copyright {
            dialog.set_copyright(Some(copyright));
        }

        if let Some(license) = &settings.license {
            dialog.set_license(Some(license));
        }

        if let Some(logo) = &settings.logo {
            dialog.set_logo(Some(logo));
        }

        if let Some(logo_icon_name) = &settings.logo_icon_name {
            dialog.set_logo_icon_name(Some(logo_icon_name));
        }

        if let Some(system_information) = &settings.system_information {
            dialog.set_system_information(Some(system_information));
        }

        if let Some(translator_credits) = &settings.translator_credits {
            dialog.set_translator_credits(Some(translator_credits));
        }

        if let Some(version) = &settings.version {
            dialog.set_version(Some(version));
        }

        if let Some(website) = &settings.website {
            dialog.set_website(Some(website));
        }

        if let Some(website_label) = &settings.website_label {
            dialog.set_website_label(website_label);
        }

        if settings.forward_activate_link {
            dialog.connect_activate_link(move |_, uri| {
                sender
                    .output(Self::Output::ActivateLink(uri.to_string()))
                    .unwrap();
                gtk::Inhibit(true)
            });
        }

        ComponentParts { model, widgets }
    }

    view! {
        #[root]
        #[name = "about_dialog"]
        gtk::AboutDialog {
            set_license_type: settings.license_type,
            set_program_name: Some(settings.program_name.as_str()),
            set_wrap_license: settings.wrap_license,
            set_modal: settings.modal,
            #[track = "model.changed(AboutDialog::visible())"]
            set_visible: model.visible,

            connect_close_request[sender] => move |_| {
                sender.input(Self::Input::Close);
                gtk::Inhibit(true)
            },
        }
    }

    fn update(&mut self, message: Self::Input, _sender: ComponentSender<Self>) {
        self.reset();
        match message {
            Self::Input::Show => self.set_visible(true),
            Self::Input::Close => self.set_visible(false),
        }
    }
}
