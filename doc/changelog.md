### Version 0.9.5 (2021-10-12 CEST)
 * Fix compatibility warnings
 * Change license to AGPL 3 and update copyright and credits

### Version 0.9.4 (2017-04-09 CEST)
 * Select SHA-256 by default

### Version 0.9.3 (2017-04-09 CEST)
 * Add '-f' '--file' command line parameter
 * Load appropriate gettext implementations on Linux, OSX and Windows
 * Fix translations not loaded bug
 * Add Makefile

### Version 0.9.2 (2015-09-05 CEST)
 * Add file command line parameter handling
 * Reset status bar on verification start
 * Update copyright notice and license info

### Version 0.9.1 (2013-07-27 CEST)
 * Add Czech translation; thanks to Pavel Fric \[[fripohled.blogspot.cz](http://fripohled.blogspot.cz)\]
 * Add accelerators to main menu items
 * Add icons to verify/cancel button
 * Fix minor bugs
