header =
  .reset_tooltip = Reset program
  .about_tooltip = Show program information

file_chooser =
  .choose_file = Choose _File
  .choose = Choose
  .cancel = Cancel

# $algorithm (String)
algorithm =
  .safe = 💚 {$algorithm}
  .moderate = 💛 {$algorithm}
  .compromised = 💔 {$algorithm}

main =
  .algorithm_tooltip = Choose checksum calculation algorithm
  .edit_placeholder = Enter checksum here
  .calculate = _Calculate
  .verify = _Verify
  .cancel = Ca_ncel

status =
  .checksums_match = Checksums match
  .checksums_do_not_match = Checksums DO NOT match

about_dialog =
  .description = Suma is a simple checksum validator.
  .copyright = Copyright © 2013-2022 Piotr Orzechowski [orzechowski.tech]
